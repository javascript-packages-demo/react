# [react](https://www.npmjs.com/package/react)

A declarative, efficient, and flexible JavaScript library for building user interfaces. https://reactjs.org

(Unofficial demo and howto)

* [Getting started With React in Symfony Using Webpack Encore](https://www.cloudways.com/blog/symfony-react-using-webpack-encore/)

# Books
* *Learning React : functional web development with React and Redux*
  2017 Alex Banks, Eve Porcello
